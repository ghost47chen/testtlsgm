module github.com/guijunchen/tls/tlsteststd

go 1.15

require (
	github.com/Hyperledger-TWGC/ccs-gm v0.1.1
	github.com/Hyperledger-TWGC/tjfoc-gm v1.4.0
	github.com/iu0v1/herots v2.1.2+incompatible
)

replace (
	github.com/Hyperledger-TWGC/tjfoc-gm => ./tjfoc-gm
)