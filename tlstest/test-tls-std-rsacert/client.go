package main

import (
	"crypto/tls"
	"crypto/x509"
	"io/ioutil"
	"log"
)

func main() {
	clientcert := "../asserts/rsa-cert/client.crt"
	clientkey := "../asserts/rsa-cert/client.key"
	cacert := "../asserts/rsa-cert/ca.crt"
	cert, err := tls.LoadX509KeyPair(clientcert, clientkey)
	if err != nil {
		log.Println(err)
		return
	}
	certBytes, err := ioutil.ReadFile(cacert)
	if err != nil {
		panic("Unable to read cert.pem")
	}
	clientCertPool := x509.NewCertPool()
	ok := clientCertPool.AppendCertsFromPEM(certBytes)
	if !ok {
		panic("failed to parse root certificate")
	}
	conf := &tls.Config{
		RootCAs:            clientCertPool,
		Certificates:       []tls.Certificate{cert},
		InsecureSkipVerify: false,
	}
	conn, err := tls.Dial("tcp", "127.0.0.1:6443", conf)
	if err != nil {
		log.Println(err)
		return
	}
	defer conn.Close()
	n, err := conn.Write([]byte("hello\n"))
	if err != nil {
		log.Println(n, err)
		return
	}
	buf := make([]byte, 100)
	n, err = conn.Read(buf)
	if err != nil {
		log.Println(n, err)
		return
	}
	println(string(buf[:n]))
}
