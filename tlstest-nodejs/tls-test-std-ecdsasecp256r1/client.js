var tls = require('tls');
var fs = require('fs');

var options = {
  // These are necessary only if using the client certificate authentication
  key: fs.readFileSync('../asserts/ecdsa-secp256r1-cert/client.key'),
  cert: fs.readFileSync('../asserts/ecdsa-secp256r1-cert/client.crt'),

  // This is necessary only if the server uses the self-signed certificate
  ca: [ fs.readFileSync('../asserts/ecdsa-secp256r1-cert/ca.crt') ]
};

var socket = tls.connect(6443, options, function() {
  console.log('client connected',
              socket.authorized ? 'authorized' : 'unauthorized');
  process.stdin.pipe(socket);
  process.stdin.resume();
});
socket.setEncoding('utf8');
socket.on('data', function(data) {
  console.log(data);
});
socket.on('end', function() {
  server.close();
});

//openssl s_client -connect 127.0.0.1:8000 -showcerts -msg  -CAfile ../asserts/rsa-cert/ca.crt -cert ../asserts/rsa-cert/client.crt -key ../asserts/rsa-cert/client.key 