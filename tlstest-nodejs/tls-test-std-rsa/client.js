var tls = require('tls');
var fs = require('fs');

var options = {
  // These are necessary only if using the client certificate authentication
  key: fs.readFileSync('../asserts/rsa-cert/client.key'),
  cert: fs.readFileSync('../asserts/rsa-cert/client.crt'),

  // This is necessary only if the server uses the self-signed certificate
  ca: [ fs.readFileSync('../asserts/rsa-cert/ca.crt') ]
};

var socket = tls.connect(8000, options, function() {
  console.log('client connected',
              socket.authorized ? 'authorized' : 'unauthorized');
  process.stdin.pipe(socket);
  process.stdin.resume();
});
socket.setEncoding('utf8');
socket.on('data', function(data) {
  console.log(data);
});
socket.on('end', function() {
  server.close();
});

//openssl s_client -connect 127.0.0.1:8000 -showcerts -msg  -CAfile ../asserts/rsa-cert/ca.crt -cert ../asserts/rsa-cert/client.crt -key ../asserts/rsa-cert/client.key 